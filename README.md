1. As per https://gitlab.com/dimpastra/oracle_soa_suite build image `oracle/soa:12.2.1.3`
2. build jdeveloper image 

```
docker build -t oracle/jdeveloper:12.2.1.4 .
```
3. start container 
```
docker run --name jdeveloper_container -idt -p 5901:5901 -p 6901:6901  -e VNC_PW=myVNCpass8989wd  oracle/jdeveloper:12.2.1.4 
```
4. go to chrome and access the container and start jdeveloper (icon on Desktop), just click on it to start

http://PUBLIC_ADDRESS:6901/?password=myVNCpass8989wd

5. if something not ok , check container logs

```
docker logs -f jdeveloper_container 
```
### References

https://github.com/ConSol/docker-headless-vnc-container
https://github.com/tkleiber/de.kleiber.docker.images.oracle.jdeveloper
